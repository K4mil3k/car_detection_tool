#ifndef ANALISEWARNING_H
#define ANALISEWARNING_H

#include <QDialog>

namespace Ui {
class AnaliseWarning;
}

class AnaliseWarning : public QDialog
{
    Q_OBJECT

public:
    explicit AnaliseWarning(QWidget *parent = 0);
    ~AnaliseWarning();

private:
    Ui::AnaliseWarning *ui;
};

#endif // ANALISEWARNING_H
