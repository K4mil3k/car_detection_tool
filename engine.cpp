#include "engine.h"

Engine::Engine()
{


}

void Engine::MainEngine()
{
    if (AnalysisCheck == 1)
    {

    VideoCapture Vid;

    Mat Frame1;
    Mat Frame2;
    Mat FrameMain;

    Vid.open(file);

    //VideoWriter video;     // Zapisywanie pliku video

    if(!Vid.isOpened()){
        cout << "Error"<< endl;
    }
    else {
        cout << "File loaded"<< endl;
    }

    Vid.read(Frame1);
    Vid.read(Frame2);

    FrameMain = FrameBackground;

    //video.open("Final Result.avi",CV_FOURCC('F','F','V','1'),20,Frame1.size());  // Zapisywanie pliku video



    while(stop == 0)
    {
        if(playback != 0)
        {
        if (!Vid.read(Frame1) || !Vid.read(Frame2)) break;
        }

        vector<Segm> segm;

        Mat Frame1C = Frame1.clone();
        Mat Frame2C = Frame2.clone();


        Mat Diff;
        Mat Thresh;


        cvtColor(Frame1C, Frame1C, CV_BGR2GRAY);
        cvtColor(Frame2C, Frame2C, CV_BGR2GRAY);

        GaussianBlur(Frame1C, Frame1C, Size(5,5),0);
        GaussianBlur(Frame2C, Frame2C, Size(5,5),0);

        absdiff(Frame1C,Frame2C,Diff);

        threshold(Diff, Thresh, 30,255,CV_THRESH_BINARY);


        // Operacje morfologiczne

        Mat strEl = getStructuringElement(MORPH_RECT, Size(5,5));
        for (int i = 0; i<2; i++){
        dilate(Thresh, Thresh,strEl);
        dilate(Thresh, Thresh,strEl);
        erode(Thresh,Thresh,strEl);}

        Mat ThreshC = Thresh.clone();


        // Pobranie konturów

        vector<vector<Point> > contours;
        findContours(ThreshC, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
        Mat Contours(Thresh.size(), CV_8UC3, Black);
        drawContours(Contours, contours, -1, White, -1);


        // Powłoki wypukłe

        vector<vector<Point> > convexHulls(contours.size());

        for (int i = 0; i < contours.size(); i++){
            convexHull(contours[i],convexHulls[i]);
        }

        for (auto &convexHull : convexHulls) {
            Segm posSegm(convexHull);

            if (posSegm.boundRect.area() > 400 && posSegm.dblAspectRatio >= 0.2 && posSegm.dblAspectRatio <= 4.0 && posSegm.boundRect.width > 30 && posSegm.boundRect.height > 30 && posSegm.dblDiagonalSize > 60.0 && (contourArea(posSegm.contour)/(double)(posSegm.boundRect.area()) > 0.50))
            {
            segm.push_back(posSegm);
            }
        }

        Mat iConvexHulls(Thresh.size(), CV_8UC1, Black);
        convexHulls.clear();

        for (auto &seg : segm){
            convexHulls.push_back(seg.contour);
        }

        drawContours(iConvexHulls, convexHulls, -1, White, -1);

        Mat FrameIn = Frame2.clone();
        Mat test;
        Mat FrameCut;


        // INPAINT (OBSOLETE), pozostawione jako ślad prób wykorzystania metody

        /*Mat inpaintMask;
        iConvexHulls.convertTo(inpaintMask, CV_8U);
        inpaint(FrameIn, inpaintMask, FrameIn, 4,INPAINT_TELEA);
        imshow("inpaint", FrameIn );*/

        // INPAINT OVER


        // Wklejanie fragmetnów tła

        Mat inv;
        bitwise_not(iConvexHulls,inv);
        bitwise_and(FrameIn,FrameIn,test,inv);
        FrameMain.copyTo(FrameCut,iConvexHulls);
        test = test+FrameCut;


        // Wyświetlanie finalnego efektu

        imshow("Car Erasing App",test);


        // video.write(test);     // Zapisywanie pliku video


        char c = waitKey(FrameTime);

    }

    destroyAllWindows();

   }

    else {
        cout << "Click 'Analise Video' first" << endl;
    }


}

void Engine::AnalysisEngine()
{

    AnalysisCheck = 1;
    VideoCapture Vid;
    Mat Frame1;
    nof = 1;
    deque<double> PixVal;

    Vid.open(file);

    if(!Vid.isOpened()){
        cout << "Error"<< endl;

    }
    else {
        cout << "File loaded"<< endl;
    }

    Vid.read(Frame1);


    FrameBackground = Frame1.clone();
    FrameBackground.setTo(0);


    int channels = Frame1.channels();
    int rows = Frame1.rows;
    int cols = Frame1.cols * channels;
    
    if (Frame1.isContinuous())
    {
        cols *= rows;
        rows = 1;
    }

    uchar* p;
    uchar* t;

   for (int i = 0; i<rows; i++)
    {
        p = Frame1.ptr<uchar>(i);
        t = FrameBackground.ptr<uchar>(i);
        for (int j = 0; j<cols; j++)
        {
        PixVal.push_back(p[j]);

        }


}

    while (Vid.read(Frame1))
    {


    for (int i = 0; i<rows; i++)
    {
        p = Frame1.ptr<uchar>(i);
        t = FrameBackground.ptr<uchar>(i);
        for (int j = 0; j<cols; j++)
        {
           PixVal.at(j) += p[j];
        }
    }
    nof++;
}

    for (int i = 0; i<rows; i++)
    {
        p = Frame1.ptr<uchar>(i);
        t = FrameBackground.ptr<uchar>(i);
        for (int j = 0; j<cols; j++)
        {
        t[j] = PixVal.at(j)/nof;
        }
    }
    imshow("lel", FrameBackground);
    imwrite("Bckgrnd.jpg", FrameBackground);





}

void Engine::FastAnalysisEngine()
{
    AnalysisCheck = 1;
    VideoCapture Vid;
    Mat Frame1;
    nof = 1;



    Vid.open(file);

    if(!Vid.isOpened()){
        cout << "Error"<< endl;

    }
    else {
        cout << "File loaded"<< endl;
    }

    Vid.read(Frame1);

    FrameBackground = Mat(Frame1.size(), CV_32FC3, Scalar(0,0,0));

    while (Vid.read(Frame1))
    {
        Mat asd;
        Frame1.convertTo(asd, CV_32FC3);
        FrameBackground += asd;
        nof++;
    }

    FrameBackground /= nof;
    FrameBackground.convertTo(FrameBackground, CV_8UC3);
    imshow("bg", FrameBackground);
    imwrite("bckgrndFAST.jpg",FrameBackground);





}

void Engine::stopPlayback()
{
    playback = 0;
}

void Engine::startPlayback()
{
    playback = 1;
}

void Engine::setStop()
{
    stop = 1;
}

void Engine::resetStop()
{
    stop = 0;
}

void Engine::setFrameTime(int Time)
{
    FrameTime = Time;
}

int Engine::getNOF()
{
    return nof;
}

