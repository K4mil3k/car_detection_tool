#-------------------------------------------------
#
# Project created by QtCreator 2017-06-17T17:23:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WMAProjWINDOW
TEMPLATE = app

INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_videoio -lopencv_imgproc -lopencv_photo


SOURCES += main.cpp\
        mainwindow.cpp \
    segm.cpp \
    engine.cpp \
    warningdialog.cpp \
    analisewarning.cpp

HEADERS  += mainwindow.h \
    segm.h \
    engine.h \
    warningdialog.h \
    analisewarning.h

FORMS    += mainwindow.ui \
    warningdialog.ui \
    analisewarning.ui
