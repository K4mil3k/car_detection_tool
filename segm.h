#ifndef SEGM_H
#define SEGM_H

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

class Segm
{
    public:
    vector <Point> contour;
    Rect boundRect;
    Point centerPos;
    double dblDiagonalSize;
    double dblAspectRatio;

    Segm(vector<Point> _contour);
};

#endif // SEGM_H
