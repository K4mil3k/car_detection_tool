#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_2_clicked()
{
    int Time = ui->FrameTime->value();
    engine.setFrameTime(Time);
    engine.resetStop();
    engine.startPlayback();
    if (engine.AnalysisCheck == 1)
    {
    engine.MainEngine();
    }
    else
    {
        AnaliseWarning dialog;
        dialog.setModal(true);
        dialog.exec();

    }
}

void MainWindow::on_pushButton_3_clicked()
{
    engine.stopPlayback();
}

void MainWindow::on_pushButton_4_clicked()
{
    engine.startPlayback();
}

void MainWindow::on_pushButton_5_clicked()
{
    engine.setStop();
    engine.stopPlayback();
}

void MainWindow::on_pushButton_clicked()
{
    WarningDialog dialog;
    dialog.setModal(true);
    dialog.exec();
    engine.AnalysisEngine();

}



void MainWindow::on_pushButton_6_clicked()
{
    WarningDialog dialog;
    dialog.setModal(true);
    dialog.exec();
    engine.FastAnalysisEngine();
}
