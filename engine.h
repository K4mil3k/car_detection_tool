#ifndef ENGINE_H
#define ENGINE_H

#include <QCoreApplication>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo/photo.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <QDebug>
#include <segm.h>
#include <deque>

using namespace std;
using namespace cv;


class Engine
{
public:
    Engine();
    void MainEngine();
    void AnalysisEngine();
    void FastAnalysisEngine();
    void FrameScan();


    void stopPlayback();
    void startPlayback();
    void setStop();
    void resetStop();
    void setFrameTime(int Time);
    int getNOF();


    string file = "Cars.mp4";
    bool showInpaint;
    bool stop;
    bool playback;
    bool AnalysisCheck = 0;
    bool permission = 0;
    int FrameTime;
    Mat FrameBackground;
    int nof;




    const Scalar Black = Scalar(0,0,0);
    const Scalar White = Scalar(255,255,255);
    const Scalar Blue = Scalar(255,0,0);


};

#endif // ENGINE_H
