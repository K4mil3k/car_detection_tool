#include "segm.h"

Segm::Segm(vector<Point> _contour)
{
contour = _contour;
boundRect = boundingRect(contour);

centerPos.x = (boundRect.x + boundRect.x + boundRect.width) / 2;
centerPos.y = (boundRect.y + boundRect.y + boundRect.height) / 2;

dblDiagonalSize = sqrt(pow(boundRect.width, 2) + pow(boundRect.height, 2));

dblAspectRatio = (float)boundRect.width / (float)boundRect.height;

}
